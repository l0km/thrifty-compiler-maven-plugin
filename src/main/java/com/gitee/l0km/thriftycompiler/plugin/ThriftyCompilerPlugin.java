package com.gitee.l0km.thriftycompiler.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.microsoft.thrifty.compiler.ThriftyCompiler;

/**
 * Microsoft/thrifty-compiler maven执行插件<br>
 * 根据thrift IDL接口文件生成client端存根代码(java|kotlin)<br>
 * NOTE: JDK 8 required<br>
 * command line example:<br>
 * <pre>
 *   mvn com.gitee.l0km:thrifty-compiler-maven-plugin:1.2:stub 
 *   	-DoutputDir=/your/output/path 
 *   	-DthriftFiles=your.thrift
 *   </pre>
 * @author guyadong
 *
 */
@Mojo(name = "stub", requiresProject = false)
public class ThriftyCompilerPlugin extends AbstractMojo {
	/**
	 * the output directory for generated files
	 */
	@Parameter(property="outputDir",defaultValue = "${project.build.directory}/generated-sources")
	private File outputDir;
	/**
	 * the search path for .thrift includes
	 */
	@Parameter(property="path")
	private String path;
	/**
	 * the target language for generated code.Default is java.[java|kotlin]
	 */
	@Parameter(property="lang",defaultValue="java")
	private String lang;
	/**
	 * Format style for generated names.  Default is to leave names unaltered. [default|java]
	 */
	@Parameter(property="nameStyle",defaultValue="default")
	private String nameStyle;
	/**
	 * when specified, the concrete type to use for lists
	 */
	@Parameter(property="listType")
	private String listType;
	/**
	 * when specified, the concrete type to use for sets
	 */
	@Parameter(property="setType")
	private String setType;
	/**
	 * when specified, the concrete type to use for maps
	 */
	@Parameter(property="mapType")
	private String mapType;
	/**
	 * When set, will add android.support nullability annotations to fields
	 */
	@Parameter(property="useAndroidAnnotations")
	private boolean useAndroidAnnotations;
	/**
	 * When set, generates Parcelable implementations for structs
	 */
	@Parameter(property="parcelable")
	private boolean parcelable;
	/**
	 * When set, don't add file comments to generated files
	 */
	@Parameter(property="omitFileAnnotations")
	private boolean omitFileAnnotations;
	/**
	 * When set, @Generated annotations will be suppressed
	 */
	@Parameter(property="omitGeneratedAnnotations")
	private boolean omitGeneratedAnnotations = true;
	/**
	 * JDK 9 repackaged the traditional @Generated annotation. 
	 * The current platform's annotation is used by default, 
	 * unless overridden with this option.[jdk8|jdk9|native]
	 */
	@Parameter(property="generatedAnnotationType")
	private String generatedAnnotationType;
	/**
	 * Generate one .kt file per type; default is one per namespace.
	 */
	@Parameter(property="ktFilePerType")
	private boolean ktFilePerType;
	/**
	 * 
	 */
	@Parameter(property="experimentalKtBuilderlessStructs")
	private boolean experimentalKtBuilderlessStructs;
	/**
	 * 
	 */
	@Parameter(property="ktCoroutineClients")
	private boolean ktCoroutineClients;
	/**
	 * All .thrift files to compile
	 */
	@Parameter(property="thriftFiles")
	private List<String> thriftFiles = ImmutableList.of();
	/**
	 * Show this message and exit
	 */
	@Parameter(property="help")
	private boolean help;
	public ThriftyCompilerPlugin() {
	}

	private List<String> makeArgs() {
		ArrayList<String> args = new ArrayList<String>();
		if (help) {
			args.add("--help");
			return args;
		}

		if (outputDir!=null) {
			args.add("--out");
			args.add(this.outputDir.getAbsolutePath());
			getLog().info("outputDir=" + this.outputDir.getAbsolutePath());
		}
		if (!Strings.isNullOrEmpty(path)) {
			args.add("--path");
			args.add(path);
			getLog().info("path=" + this.path);
		}
		if (!Strings.isNullOrEmpty(lang)) {
			args.add("--lang");
			args.add(this.lang);
			getLog().info("language=" + this.lang);

		}
		if (!Strings.isNullOrEmpty(nameStyle)) {
			args.add("--name-style");
			args.add(this.nameStyle);
			getLog().info("nameStyle" + this.nameStyle);

		}
		if (!Strings.isNullOrEmpty(listType)) {
			args.add("--list-type");
			args.add(this.listType);
			getLog().info("listType=" + this.listType);

		}
		if (!Strings.isNullOrEmpty(setType)) {
			args.add("--set-type");
			args.add(this.setType);
			getLog().info("setType=" + this.setType);
		}
		if (!Strings.isNullOrEmpty(mapType)) {
			args.add("--map-type");
			args.add(this.mapType);
			getLog().info("mapType=" + this.mapType);
		}
		if (useAndroidAnnotations) {
			args.add("--use-android-annotations");
			getLog().info("useAndroidAnnotations=true");
		}
		if (parcelable) {
			args.add("--parcelable");
			getLog().info("parcelable=true");
		}
		if (omitFileAnnotations) {
			args.add("--omit-file-comments");
			getLog().info("omitFileAnnotations=true");
		}
		if(omitGeneratedAnnotations) {
			args.add("--omit-generated-annotations");
			getLog().info("omitGeneratedAnnotations=true");
		}
		if (!Strings.isNullOrEmpty(generatedAnnotationType)) {
			args.add("--generated-annotation-type");
			args.add(this.generatedAnnotationType);
			getLog().info("generatedAnnotationType=" + this.generatedAnnotationType);
		}
		if (ktFilePerType) {
			args.add("--kt-file-per-type");
			getLog().info("ktFilePerType=true");
		}
		if (experimentalKtBuilderlessStructs) {
			args.add("--experimental-kt-builderless-structs");
			getLog().info("experimentalKtBuilderlessStructs=true");
		}
		if (ktCoroutineClients) {
			args.add("--kt-coroutine-clients");
			getLog().info("ktCoroutineClients=true");
		}
		for(String name:thriftFiles){
			String file = new File(name).getAbsolutePath();
			args.add(file);
			getLog().info("thriftFiles:" + file);
		}
		return args;
	}
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		try{
			getLog().info("调用Microsoft/thrifty compiler生成client端存根代码");
			ThriftyCompiler.main(makeArgs().toArray(new String[0]));
		}catch(RuntimeException e){
			throw new MojoExecutionException(e.getMessage(),e);
		}
	}
}
