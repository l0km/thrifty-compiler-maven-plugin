# thrifty-compiler-maven-plugin

[Microsoft/thrifty-compiler](https://github.com/Microsoft/thrifty/tree/master/thrifty-compiler)是
Microsoft的[thrifty](https://github.com/Microsoft/thrifty)框架的代码生成器，可以根据IDL(接口定义文件)生成client端的stub代码。是一个支持命令行运行的java程序。

然而thrifty官方并没提供对应的maven插件，所以不能在maven 的pom.xml中调用thrifty-compiler.对于习惯使用maven进行开发管理的用户来说确实有些不方便。thrifty-compiler-maven-plugin就是为了解决这个问题而开发的。

thrifty-compiler-maven-plugin将 thrifty-compiler 封装为一个Maven插件，允许在命令行或pom中执行thrifty-compiler生成stub代码

thrifty-compiler的官方参数说明参见 [https://github.com/Microsoft/thrifty/tree/master/thrifty-compiler
](https://github.com/Microsoft/thrifty/tree/master/thrifty-compiler)

**NOTE:** JDK 8 required
	

#### 命令行调用示例

直接在命令行中如下输入命令，即可生成stub代码

	  mvn com.gitee.l0km:thrifty-compiler-maven-plugin:1.0:stub \
	    -DoutputDir=/your/output/path \
	    -DthriftFiles=your.thrift \

#### pom.xml示例

插件在pom.xml中调用如下：

	<project>
		....
		<build>
		...
		</build>
		<profiles>
			<profile>
				<id>thrifty-stub</id>
				<!-- thrifty-compiler 需要jdk8 -->
				<activation>
					<jdk>[1.8,)</jdk>
				</activation>
				<build>
					<plugins>
						<plugin>
							<groupId>com.gitee.l0km</groupId>
							<artifactId>thrifty-compiler-maven-plugin</artifactId>
							<version>1.0</version>
							<executions>
								<execution>
									<goals>
										<goal>stub</goal>
									</goals>
								</execution>
							</executions>
							<configuration>
								<outputDir>${project.basedir}/src/thrifty/java</outputDir>
								<thriftFiles>
									<thriftFile>${project.basedir}/../FaceApi.thrift</thriftFile>
								</thriftFiles>
							</configuration>
						</plugin>
					</plugins>
				</build>
			</profile>
		</profiles>
	</project>

命令行执行：

	mvn com.gitee.l0km:thrifty-compiler-maven-plugin:1.0-SNAPSHOT:stub

输出：

	[INFO] Scanning for projects...
	[INFO]
	[INFO] ------------------------------------------------------------------------
	[INFO] Building faceapi thrift client for android 1.0.6-SNAPSHOT
	[INFO] ------------------------------------------------------------------------
	[INFO]
	[INFO] --- thrifty-compiler-maven-plugin:1.0-SNAPSHOT:stub (default-cli) @ faceapi-thriftclient-android ---
	[INFO] 调用Microsoft/thrifty compiler生成client端存根代码
	[INFO] outputDir=J:\faceapi\faceapi-thriftclient-android\src\thrifty\java
	[INFO] language=java
	[INFO] nameStyle=java
	[INFO] thriftFiles:J:\faceapi\faceapi-thriftclient-android\..\FaceApi.thrift
	[INFO] ------------------------------------------------------------------------
	[INFO] BUILD SUCCESS
	[INFO] ------------------------------------------------------------------------
	[INFO] Total time: 1.092 s
	[INFO] Finished at: 2019-02-13T13:22:29+08:00
	[INFO] Final Memory: 11M/313M
	[INFO] ------------------------------------------------------------------------

#### 参数说明

thrifty-compiler-maven-plugin与thrifty-compiler基本相同,执行

	mvn help:describe -Dplugin=com.gitee.l0km:thrifty-compiler-maven-plugin:1.0-SNAPSHOT -Dgoal=stub -Ddetail

即可显示参数信息:

	experimentalKtBuilderlessStructs
	  User property: experimentalKtBuilderlessStructs
	  (no description available)
	
	generatedAnnotationType
	  JDK 9 repackaged the traditional @Generated annotation. The current
	  platform's annotation is used by default, unless overridden with this
	  option.[jdk8|jdk9|native]
	
	help
	  Show this message and exit
	
	ktCoroutineClients
	  User property: ktCoroutineClients
	  (no description available)
	
	ktFilePerType
	  Generate one .kt file per type; default is one per namespace.
	
	lang (Default: java)
	  the target language for generated code.Default is java.[java|kotlin]
	
	listType
	  when specified, the concrete type to use for lists
	
	mapType
	  when specified, the concrete type to use for maps
	
	nameStyle (Default: java)
	  Format style for generated names. Default is to leave names unaltered.
	  [default|java]
	
	omitFileAnnotations
	  When set, don't add file comments to generated files
	
	omitGeneratedAnnotations
	  When set, @Generated annotations will be suppressed
	
	outputDir (Default: ${project.build.directory}/generated-sources)
	  the output directory for generated files
	
	parcelable
	  When set, generates Parcelable implementations for structs
	
	path
	  the search path for .thrift includes
	
	setType
	  when specified, the concrete type to use for sets
	
	thriftFiles
	  All .thrift files to compile
	
	useAndroidAnnotations
	  When set, will add android.support nullability annotations to fields


也可以执行

	mvn com.gitee.l0km:thrifty-compiler-maven-plugin:1.0-SNAPSHOT:stub -Dhelp

显示thrifty-compiler的原始帮助信息。

